/**
 * Created by Gosia on 27/10/2015.
 */
public class Account {
    private String number;
    private History history = new History();
    private double amount;
    private HistoryLog.OperationType operationType;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public History getHistory() {
        return history;
    }

    public void setHistory(History history) {
        this.history = history;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    public void add(double amount){
        this.operationType = HistoryLog.OperationType.income;
        this.amount += amount;
    }
    public void less(double amount){
        this.operationType = HistoryLog.OperationType.outcome;
        this.amount -= amount;
    }

    public void DoOperation(Operation operation){
        operation.execute(this);
        this.history.addHistoryLogList(operation.getName(), operationType);
    }
}
