/**
 * Created by Gosia on 27/10/2015.
 */
public class Transfer implements Operation{
    private double amount;
    private Account from;
    private Account to;

    public void setTo(Account to) {
        this.to = to;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String getName() {
        return "Ciastko - Transfer";
    }

    @Override
    public void execute(Account account) {
        setFrom(account);
        from.less(amount);
        to.add(amount);
    }
}
