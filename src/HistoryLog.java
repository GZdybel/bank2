import java.util.Date;

/**
 * Created by Gosia on 27/10/2015.
 */
public class HistoryLog {
    private String title;
    private String date;
    private OperationType operationType;
    public enum OperationType {
        income, outcome
    }

    public HistoryLog(String title, OperationType operationType) {
        this.date = getDateOdOperation();
        this.title = title;
        this.operationType = operationType;
    }
    public String getDateOdOperation(){
        Date dateOdOperation = new Date();
        return dateOdOperation.toString();
    }

    public String getTitle() {
        return title;
    }
    public void everything(){
        System.out.println(this.title);
        System.out.println(this.date);
        System.out.println(this.operationType);
    }

}
