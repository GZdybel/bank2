/**
 * Created by Gosia on 27/10/2015.
 */
public class Bank {
    public static void main(String[] args) {
        Account account1 = new Account();
        Account account2 = new Account();

        account1.setAmount(100);
        account2.setAmount(30);

        Income income1 = new Income();
        income1.setAmount(10);
        Income income2 = new Income();
        income2.setAmount(16);

        Transfer transfer1 = new Transfer();
        transfer1.setAmount(3);
        transfer1.setTo(account2);
        Transfer transfer2 = new Transfer();
        transfer2.setAmount(99);
        transfer2.setTo(account1);


        account1.DoOperation(income1);
        History account1history = account1.getHistory();
        account1history.showHistoryLog();

        System.out.println(account1.getAmount());


        account2.DoOperation(income2);
        History account2history = account2.getHistory();
        account2history.showHistoryLog();

        System.out.println(account2.getAmount());

        account1.DoOperation(transfer1);
        account1history.showHistoryLog();

        System.out.println(account1.getAmount());


        account2.DoOperation(transfer2);
        account2history.showHistoryLog();

        System.out.println(account2.getAmount());

    }
}
