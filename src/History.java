import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Gosia on 27/10/2015.
 */
public class History {
    private List<HistoryLog> historyLogList = new ArrayList<HistoryLog>();

    public void addHistoryLogList(String title, HistoryLog.OperationType operationType){
        historyLogList.add(new HistoryLog(title, operationType));
    }
    public void showHistoryLog(){
        for (HistoryLog next : historyLogList) {
            next.everything();
        }

    }
}
