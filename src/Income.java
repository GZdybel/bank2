/**
 * Created by Gosia on 27/10/2015.
 */
public class Income implements Operation {

    private double amount;

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String getName() {
        return "Ciastko - Income";
    }

    @Override
    public void execute(Account account) {
        account.add(amount);
    }
}
