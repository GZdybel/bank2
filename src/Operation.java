/**
 * Created by Gosia on 27/10/2015.
 */
public interface Operation {
   String getName();
   void execute(Account account);
}
